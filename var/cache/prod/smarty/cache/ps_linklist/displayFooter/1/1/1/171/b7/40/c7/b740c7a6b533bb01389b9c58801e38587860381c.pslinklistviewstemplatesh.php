<?php
/* Smarty version 3.1.32, created on 2018-10-29 14:31:29
  from 'module:pslinklistviewstemplatesh' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bd760112b1771_09649616',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906548e89c8c6025457ddaeffb1980a0c743b872' => 
    array (
      0 => 'module:pslinklistviewstemplatesh',
      1 => 1540827075,
      2 => 'module',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_5bd760112b1771_09649616 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="footer_block col-xs-12 col-sm-12 col-md-6 col-lg-2 col-xl-2 wrapper">
  <h3 class="footer_header">Productos</h3>
    <ul class="footer_list toggle-footer" id="footer_sub_menu_66483">
		  <li>
		<a
			id="link-product-page-prices-drop-1"
			class="cms-page-link"
			href="http://localhost/eecol/es/productos-rebajados"
			title="Our special products">
		  Ofertas
		</a>
	  </li>
		  <li>
		<a
			id="link-product-page-new-products-1"
			class="cms-page-link"
			href="http://localhost/eecol/es/novedades"
			title="Novedades">
		  Novedades
		</a>
	  </li>
		  <li>
		<a
			id="link-product-page-best-sales-1"
			class="cms-page-link"
			href="http://localhost/eecol/es/mas-vendidos"
			title="Los más vendidos">
		  Los más vendidos
		</a>
	  </li>
	  </ul>
</div>
<div class="footer_block col-xs-12 col-sm-12 col-md-6 col-lg-2 col-xl-2 wrapper">
  <h3 class="footer_header">Nuestra empresa</h3>
    <ul class="footer_list toggle-footer" id="footer_sub_menu_49896">
		  <li>
		<a
			id="link-cms-page-1-2"
			class="cms-page-link"
			href="http://localhost/eecol/es/content/1-entrega"
			title="Nuestros términos y condiciones de envío">
		  Envío
		</a>
	  </li>
		  <li>
		<a
			id="link-cms-page-2-2"
			class="cms-page-link"
			href="http://localhost/eecol/es/content/2-aviso-legal"
			title="Aviso legal">
		  Aviso legal
		</a>
	  </li>
		  <li>
		<a
			id="link-cms-page-3-2"
			class="cms-page-link"
			href="http://localhost/eecol/es/content/3-terminos-y-condiciones-de-uso"
			title="Nuestros términos y condiciones">
		  Términos y condiciones
		</a>
	  </li>
		  <li>
		<a
			id="link-cms-page-4-2"
			class="cms-page-link"
			href="http://localhost/eecol/es/content/4-sobre-nosotros"
			title="Averigüe más sobre nosotros">
		  Sobre nosotros
		</a>
	  </li>
		  <li>
		<a
			id="link-cms-page-5-2"
			class="cms-page-link"
			href="http://localhost/eecol/es/content/5-pago-seguro"
			title="Nuestra forma de pago segura">
		  Pago seguro
		</a>
	  </li>
		  <li>
		<a
			id="link-static-page-contact-2"
			class="cms-page-link"
			href="http://localhost/eecol/es/contactenos"
			title="Contáctenos">
		  Contacte con nosotros
		</a>
	  </li>
		  <li>
		<a
			id="link-static-page-sitemap-2"
			class="cms-page-link"
			href="http://localhost/eecol/es/mapa del sitio"
			title="¿Perdido? Encuentre lo que está buscando">
		  Mapa del sitio
		</a>
	  </li>
		  <li>
		<a
			id="link-static-page-stores-2"
			class="cms-page-link"
			href="http://localhost/eecol/es/tiendas"
			title="">
		  Tiendas
		</a>
	  </li>
	  </ul>
</div>
<?php }
}
