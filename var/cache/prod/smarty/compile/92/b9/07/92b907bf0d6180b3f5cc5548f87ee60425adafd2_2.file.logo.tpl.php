<?php
/* Smarty version 3.1.32, created on 2018-10-29 14:31:28
  from 'C:\wamp64\www\eecol\modules\poslogo\logo.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bd760108eef89_15610532',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '92b907bf0d6180b3f5cc5548f87ee60425adafd2' => 
    array (
      0 => 'C:\\wamp64\\www\\eecol\\modules\\poslogo\\logo.tpl',
      1 => 1540827067,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bd760108eef89_15610532 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="pos_logo product_block_container">
	<div class="pos_content">
		<div class="logo-slider">
			<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['logos']->value, 'logo', false, NULL, 'posLogo', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['logo']->value) {
?>
				<div class="item_logo">
					<a href ="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logo']->value['link'], ENT_QUOTES, 'UTF-8');?>
">
						<img class="img-responsive" src ="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['logo']->value['image'], ENT_QUOTES, 'UTF-8');?>
" alt ="" />
					</a>
				</div>
			<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
		</div>
	</div>
</div>
<?php }
}
