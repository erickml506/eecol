<?php
/* Smarty version 3.1.32, created on 2018-10-29 14:31:23
  from 'C:\wamp64\www\eecol\themes\pos_aboss1\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5bd7600bcdeb62_04681806',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7bd233449000cdeb54042c90b392b6ac1e13f402' => 
    array (
      0 => 'C:\\wamp64\\www\\eecol\\themes\\pos_aboss1\\templates\\page.tpl',
      1 => 1540827075,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5bd7600bcdeb62_04681806 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17324532135bd7600bc69380_02145459', 'content');
$_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_title'} */
class Block_21027311835bd7600bc6f954_15629993 extends Smarty_Internal_Block
{
public $callsChild = 'true';
public $hide = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

        <header class="page-header">
          <h1><?php 
$_smarty_tpl->inheritance->callChild($_smarty_tpl, $this);
?>
</h1>
        </header>
      <?php
}
}
/* {/block 'page_title'} */
/* {block 'page_header_container'} */
class Block_17120301875bd7600bc6b328_14652293 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21027311835bd7600bc6f954_15629993', 'page_title', $this->tplIndex);
?>

    <?php
}
}
/* {/block 'page_header_container'} */
/* {block 'page_content_top'} */
class Block_14493621065bd7600bccf8d9_01622133 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_21014581335bd7600bcd2d43_04069288 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_3420820725bd7600bccd377_74409328 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card card-block">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14493621065bd7600bccf8d9_01622133', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21014581335bd7600bcd2d43_04069288', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer'} */
class Block_3541352625bd7600bcda054_16335781 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Footer content -->
        <?php
}
}
/* {/block 'page_footer'} */
/* {block 'page_footer_container'} */
class Block_11391293315bd7600bcd80c3_06749690 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <footer class="page-footer">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3541352625bd7600bcda054_16335781', 'page_footer', $this->tplIndex);
?>

      </footer>
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_17324532135bd7600bc69380_02145459 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_17324532135bd7600bc69380_02145459',
  ),
  'page_header_container' => 
  array (
    0 => 'Block_17120301875bd7600bc6b328_14652293',
  ),
  'page_title' => 
  array (
    0 => 'Block_21027311835bd7600bc6f954_15629993',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_3420820725bd7600bccd377_74409328',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_14493621065bd7600bccf8d9_01622133',
  ),
  'page_content' => 
  array (
    0 => 'Block_21014581335bd7600bcd2d43_04069288',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_11391293315bd7600bcd80c3_06749690',
  ),
  'page_footer' => 
  array (
    0 => 'Block_3541352625bd7600bcda054_16335781',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <div id="main">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17120301875bd7600bc6b328_14652293', 'page_header_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3420820725bd7600bccd377_74409328', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11391293315bd7600bcd80c3_06749690', 'page_footer_container', $this->tplIndex);
?>


  </div>
<?php
}
}
/* {/block 'content'} */
}
